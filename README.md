# rexit_tt



## Getting started

1. First of all you need to start your PostgreSQL server
2. Create `.env` file and configure environment variables (use `.env.example` as an example)

### Run the application using Docker and docker-compose
1. Download and install `Docker` (Docker Desktop for Windows) and `docker-compose` if you are Unix-based systems user
2. Go to the application directory `cd .../rexit_tt/`
3. Run `docker-compose up --build` (`docker compose up --build` for newer and Windows versions of `docker-compose`) for the first time. Also you can use `-d` flag to run it on background
4. Open your client application (web browser, Postman etc.)
5. Use `http://localhost:8000/api/docs/` to see the API documentation

### Run the application manually
1. Install Python 3.11 and pip
2. Install any virtual environment module. In this example we will use `virtualenv`: `pip install virtualenv`
3. Create virtual environment `virtualenv myenv`
4. Activate the virtual environment:
   1. Windows: `cd env/Scripts`, `activate`, `cd ../../`
   2. Unix: `source myenv/bin/activate`
5. Change `psycopg2-binary` to `psycopg2` if you are Windows user
6. Install application requirements `pip install -r requirements.txt`
7. Comment or remove line of code `"HOST": "db",  # For docker-compose startup` in `rexit_tt/src/settings.py` and uncomment the next line `# "HOST": getenv("POSTGRES_HOST") or "localhost", # For local startup`
8. Make migrations if needed `python manage.py makemigrations`
9. Migrate changes if needed `python manage.py migrate`
10. Run server `python manage.py runserver`
11. Open your client application (web browser, Postman etc.)
12. Use `http://localhost:8000/api/docs/` to see the API documentation