from rest_framework import serializers

from base_app import models


class CSVUploadSerializer(serializers.Serializer):
    csv_file = serializers.FileField()


class HumanGetSerializer(serializers.ModelSerializer):
    age = serializers.SerializerMethodField()

    class Meta:
        model = models.Human
        fields = (
            "id",
            "category",
            "firstname",
            "lastname",
            "email",
            "gender",
            "birthDate",
            "age",
        )
        read_only_fields = ("id", "age")

    def get_age(self, obj):
        return obj.age


class HumanPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Human
        fields = "__all__"
