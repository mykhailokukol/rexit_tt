from django.urls import path

from base_app import views


urlpatterns = [
    path(
        "upload/",
        views.CSVViewSet.as_view(
            {
                "post": "create",
            }
        ),
    ),
    path(
        "list/",
        views.HumanViewSet.as_view(
            {
                "get": "list",
            }
        ),
    ),
]
