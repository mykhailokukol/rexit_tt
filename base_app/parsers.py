import csv

from rest_framework.parsers import BaseParser
from rest_framework.exceptions import ParseError


class CSVParser(BaseParser):
    media_type = "text/csv"

    def parse(self, stream, media_type=None, parser_context=None):
        try:
            data = []
            csv_data = csv.DictReader(stream)
            for row in csv_data:
                data.append(row)
            return data
        except Exception as e:
            raise ParseError(f"CSV parsing error: {e}")
