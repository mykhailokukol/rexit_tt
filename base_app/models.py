import datetime

from django.db import models


class Human(models.Model):

    GENDER_CHOICES = (
        ("male", "male"),
        ("female", "female"),
    )

    category = models.CharField(max_length=32, db_index=True)
    firstname = models.CharField(max_length=32)
    lastname = models.CharField(max_length=32)
    email = models.EmailField()
    gender = models.CharField(choices=GENDER_CHOICES, db_index=True)
    birthDate = models.DateField(db_index=True)

    @property
    def age(self) -> int:
        current_date = datetime.datetime.now()
        birth_datetime = datetime.datetime.combine(
            self.birthDate, datetime.datetime.min.time()
        )
        age_difference = current_date - birth_datetime
        age_in_years = age_difference.days // 365

        return age_in_years
