import csv
from rest_framework import viewsets, status, parsers, filters
from rest_framework.response import Response
from django_filters import rest_framework as filters

from base_app import models, serializers
from base_app import services as srv


class CSVViewSet(viewsets.ModelViewSet):
    queryset = models.Human.objects.all()
    serializer_class = serializers.CSVUploadSerializer
    parser_classes = [
        parsers.MultiPartParser,
        parsers.FormParser,
    ]

    def create(self, request, *args, **kwargs):
        file_obj = request.FILES.get("csv_file")
        if not file_obj:
            return Response(
                {"error": "No file was submitted"}, status=status.HTTP_400_BAD_REQUEST
            )

        if not file_obj.name.endswith(".csv"):
            return Response(
                {"error": "Invalid file format, must be CSV"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # Processing CSV data
        try:
            decoded_file = file_obj.read().decode("utf-8")
            csv_data = csv.DictReader(decoded_file.splitlines())
            for row in csv_data:
                serializer = serializers.HumanPostSerializer(data=row)
                if serializer.is_valid():
                    serializer.save()
                else:
                    return Response(
                        serializer.errors, status=status.HTTP_400_BAD_REQUEST
                    )

            return Response(
                {"message": "CSV data uploaded successfully"},
                status=status.HTTP_201_CREATED,
            )
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)


class HumanFilter(filters.FilterSet):
    min_age = filters.NumberFilter(field_name="age", lookup_expr="gte")
    max_age = filters.NumberFilter(field_name="age", lookup_expr="lte")

    class Meta:
        model = models.Human
        fields = ["category", "gender", "birthDate", "min_age", "max_age"]


class HumanViewSet(viewsets.ModelViewSet):
    queryset = models.Human.objects.all()
    serializer_class = serializers.HumanGetSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = HumanFilter
    pagination_class = srv.Pagination
